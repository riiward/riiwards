<?php
$language['pt']['daily_report'] = 'RELATÓRIO DIÁRIO DE';
$language['pt']['summary'] = 'Sumário';
$language['pt']['yesterday'] = 'Ontem';
$language['pt']['year_to_date'] = 'Do Início Do Ano Até Ŕ Data';
$language['pt']['all'] = 'Tudo';
$language['pt']['subscribers'] = 'Subscritores';
$language['pt']['monthly_last_3_months'] = 'Mensal para os últimos 3 meses';
$language['pt']['new_signup_yesterday'] = 'Emails de Novas Inscriçőes de Ontem:';
$language['pt']['email'] = 'Email';
$language['pt']['no_email_sent_yesterday'] = 'NĂO FOI ENVIADO QUALQUER EMAIL DE ANIVERSÁRIO ONTEM';
$language['pt']['email_sent_yesterday'] = 'EMAILS DE ANIVERSÁRIO ENVIADOS ONTEM';
$language['pt']['subscriber_email'] = 'Subscritor de Email';
$language['pt']['birthday'] = 'Aniversário';
$language['pt']['sent_on'] = 'Enviado em';
$language['pt']['email_subject'] = 'Relatório Diário De';
$language['pt']['email_body'] = 'ESTE EMAIL É GERADO AUTOMATICAMENTE. POR FAVOR NĂO RESPONDA AO EMAIL. O email de suporte é support@Riiwards.com';
$language['pt']['bday_reward_sent'] = 'Presentes De Aniversário Enviados';
$language['pt']['congrats_msg'] = "Congratulations! You have |total_cust| signups for your Riiwards Birthday Club. ";
$language['pt']['subscriber_limit_msg'] = "Please note that you will need to upgrade to Premium plan when you reach |subscriber_limit| signups.";


