<?php
$language['en']['daily_report'] = 'DAILY REPORT FOR ';
$language['en']['summary'] = 'Summary';
$language['en']['yesterday'] = 'Yesterday';
$language['en']['year_to_date'] = 'Year To Date';
$language['en']['all'] = 'All';
$language['en']['subscribers'] = 'Subscribers';
$language['en']['monthly_last_3_months'] = 'Monthly for last 3 months';
$language['en']['new_signup_yesterday'] = 'Emails of New Sign Ups Yesterday:';
$language['en']['email'] = 'Email';
$language['en']['no_email_sent_yesterday'] = 'NO BIRTHDAY EMAIL SENT YESTERDAY';
$language['en']['email_sent_yesterday'] = 'BIRTHDAY EMAILS SENT YESTERDAY';
$language['en']['subscriber_email'] = 'Subscriber Email';
$language['en']['birthday'] = 'Birthday';
$language['en']['sent_on'] = 'Sent on';
$language['en']['email_subject'] = 'Daily Report for ';
$language['en']['email_body'] = 'THIS EMAIL IS AUTOMATICALLY GENERATED. PLEASE DON\'T REPLY. For help email support@Riiwards.com';
$language['en']['bday_reward_sent'] = 'Birthday Rewards Sent';
$language['en']['congrats_msg'] = "Congratulations! You have |total_cust| signups for your Riiwards Birthday Club. ";
$language['en']['subscriber_limit_msg'] = "Please note that you will need to upgrade to Premium plan when you reach |subscriber_limit| signups.";


