<?php
$language['fr']['daily_report'] = 'RAPPORT QUOTIDIEN POUR ';
$language['fr']['summary'] = 'Résumé';
$language['fr']['yesterday'] = 'Hier';
$language['fr']['year_to_date'] = 'Année a jour';
$language['fr']['all'] = 'Tous';
$language['fr']['subscribers'] = 'Abonnés';
$language['fr']['monthly_last_3_months'] = 'Mensuel pour les 3 derniers mois';
$language['fr']['new_signup_yesterday'] = 'Emails de nouveaux abonnés d\'hier:';
$language['fr']['email'] = 'Email';
$language['fr']['no_email_sent_yesterday'] = 'PAS D\'EMAIL D\'ANNIVERSAIRE ENVOYE HIER';
$language['fr']['email_sent_yesterday'] = 'LES EMAILS D\'ANNIVERSAIRE ENVOYES HIER';
$language['fr']['subscriber_email'] = 'L\'email d\'abonné';
$language['fr']['birthday'] = 'Anniversaire';
$language['fr']['sent_on'] = 'Envoyé le';
$language['fr']['email_subject'] = 'Rapport quotidien pour ';
$language['fr']['email_body'] = 'CET EMAIL EST GENERE AUTOMATIQUEMENT. VEUILLEZ NE PAS REPONDRE. Pour de l\'aide, contactez support@Riiwards.com';
$language['fr']['bday_reward_sent'] = 'Les récompenses d\'anniversaire envoyées';
$language['en']['congrats_msg'] = "Congratulations! You have |total_cust| signups for your Riiwards Birthday Club. ";
$language['en']['subscriber_limit_msg'] = "Please note that you will need to upgrade to Premium plan when you reach |subscriber_limit| signups.";

