<?php
$language['po']['daily_report'] = 'CODZIENNY RAPORT O';
$language['po']['summary'] = 'Podsumowanie';
$language['po']['yesterday'] = 'Wczoraj';
$language['po']['year_to_date'] = 'Od Początku Roku';
$language['po']['all'] = 'Wszystko';
$language['po']['subscribers'] = 'Abonenci';
$language['po']['monthly_last_3_months'] = 'Miesięcznie za ostatnie 3 miesiące';
$language['po']['new_signup_yesterday'] = 'Wiadomości e-mail o Nowych Rejestracjach od Wczoraj:';
$language['po']['email'] = 'E-mail';
$language['po']['no_email_sent_yesterday'] = 'WCZORAJ NIE WYSŁANO MAILA O URODZINACH';
$language['po']['email_sent_yesterday'] = 'WCZORAJ WYSŁANO MAILA O URODZINACH';
$language['po']['subscriber_email'] = 'Abonent Email';
$language['po']['birthday'] = 'Urodziny';
$language['po']['sent_on'] = 'Wysłany';
$language['po']['email_subject'] = 'Raport Dzienny ';
$language['po']['email_body'] = 'TEN EMAIL JEST GENEROWANY AUTOMATYCZNIE. PROSIMY NA NIEGO NIE ODPOWIADAĆ. Aby uzyskać pomoc wyślij e-mail na support@Riiwards.com';
$language['po']['bday_reward_sent'] = 'Nagrody Urodzinowe Wysłano';
$language['en']['congrats_msg'] = "Congratulations! You have |total_cust| signups for your Riiwards Birthday Club. ";
$language['en']['subscriber_limit_msg'] = "Please note that you will need to upgrade to Premium plan when you reach |subscriber_limit| signups.";

