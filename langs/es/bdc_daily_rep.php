<?php
$language['es']['daily_report'] = 'REPORTE DIARIO PARA ';
$language['es']['summary'] = 'Resumen';
$language['es']['yesterday'] = 'Ayer';
$language['es']['year_to_date'] = 'El último ańo';
$language['es']['all'] = 'Todo';
$language['es']['subscribers'] = 'Suscriptores';
$language['es']['monthly_last_3_months'] = 'Mensual de los últimos 3 meses';
$language['es']['new_signup_yesterday'] = 'Suscriptores inscritos el día de ayer:';
$language['es']['email'] = 'Dirección de correo electrónico';
$language['es']['no_email_sent_yesterday'] = 'AYER NO SE ENVIARON CORREOS DE CUMPLAŃOS';
$language['es']['email_sent_yesterday'] = 'AYER SE ENVIARON CORREOS DE CUMPLEAŃOS';
$language['es']['subscriber_email'] = 'Dirección de correo electrónico del suscriptor';
$language['es']['birthday'] = 'Cumpleańos';
$language['es']['sent_on'] = 'Enviado el';
$language['es']['email_subject'] = 'Reporte Diario para ';
$language['es']['email_body'] = 'ESTE MENSAJE FUE GENERADO DE FORMA AUTOMATICA. POR FAVOR NO RESPONDA A ESTE MENSAJE. Si necesita ayuda escriba a support@Riiwards.com';
$language['es']['bday_reward_sent'] = 'Recompensa de Cumpleańos Enviada ';
$language['es']['congrats_msg'] = "Congratulations! You have |total_cust| signups for your Riiwards Birthday Club. ";
$language['es']['subscriber_limit_msg'] = "Please note that you will need to upgrade to Premium plan when you reach |subscriber_limit| signups.";


