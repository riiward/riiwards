<?php
$language['tr']['daily_report'] = 'ŢU KULLANICININ GÜNLÜK RAPORU: ';
$language['tr']['summary'] = 'Özet';
$language['tr']['yesterday'] = 'Dün';
$language['tr']['year_to_date'] = 'Senenin baţýndan bugüne kadar';
$language['tr']['all'] = 'Tümü';
$language['tr']['subscribers'] = 'Aboneler';
$language['tr']['monthly_last_3_months'] = 'Son 3 ay için aylýk';
$language['tr']['new_signup_yesterday'] = 'Dün Yeni Kaydolanlarýn E-posta Adresleri:';
$language['tr']['email'] = 'E-posta';
$language['tr']['no_email_sent_yesterday'] = 'DÜN DOĐUM GÜNÜ E-POSTASI GÖNDERÝLMEDÝ';
$language['tr']['email_sent_yesterday'] = 'DÜN GÖNDERÝLEN DOĐUM GÜNÜ E-POSTALARI';
$language['tr']['subscriber_email'] = 'Abone E-postasý';
$language['tr']['birthday'] = 'Dođum Günü';
$language['tr']['sent_on'] = 'Ţu tarihte gönderildi';
$language['tr']['email_subject'] = 'Ţu Kullanýcýnýn Günlük Raporu:  ';
$language['tr']['email_body'] = 'BU E-POSTA OTOMATÝK OLARAK OLUŢTURULMUŢTUR. LÜTFEN YANITLAMAYINIZ. Yardým için e-posta atýnýz: support@Riiwards.com';
$language['tr']['bday_reward_sent'] = 'Gönderilen Dođum Günü Ödülleri';
$language['tr']['congrats_msg'] = "Congratulations! You have |total_cust| signups for your Riiwards Birthday Club. ";
$language['tr']['subscriber_limit_msg'] = "Please note that you will need to upgrade to Premium plan when you reach |subscriber_limit| signups.";


