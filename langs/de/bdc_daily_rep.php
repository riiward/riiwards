<?php
$language['de']['daily_report'] = 'Täglicher Bericht für ';
$language['de']['summary'] = 'Zusammenfassung';
$language['de']['yesterday'] = 'Gestern';
$language['de']['year_to_date'] = 'Jahr zu Datum';
$language['de']['all'] = 'Alle';
$language['de']['subscribers'] = 'Abonnenten';
$language['de']['monthly_last_3_months'] = 'Monatlich für die letzten 3 Monate';
$language['de']['new_signup_yesterday'] = 'E-mails der neuen Abonnenten von Gestern:';
$language['de']['email'] = 'Email';
$language['de']['no_email_sent_yesterday'] = 'Gestern keine Geburtstags-E-Mail versendet';
$language['de']['email_sent_yesterday'] = 'Gestern versendete Geburtstags-E-Mails';
$language['de']['subscriber_email'] = 'Abonnenten-Email';
$language['de']['birthday'] = 'Geburtstag';
$language['de']['sent_on'] = 'Gesendet am';
$language['de']['email_subject'] = 'Täglicher Bericht für ';
$language['de']['email_body'] = 'Diese E-Mail wurde automatisch generiert, bitte nicht Antworten. Für Hilfe bitt an die E-Mail support@Riiwards.com wenden';
$language['de']['bday_reward_sent'] = 'Versendete Geburtstagsbelohnungen';
$language['de']['congrats_msg'] = "Congratulations! You have |total_cust| signups for your Riiwards Birthday Club. ";
$language['de']['subscriber_limit_msg'] = "Please note that you will need to upgrade to Premium plan when you reach |subscriber_limit| signups.";

