<?php
$language['ru']['daily_report'] = 'ДНЕВНОЙ ОТЧЕТ ДЛЯ ';
$language['ru']['summary'] = 'Обзор';
$language['ru']['yesterday'] = 'Вчера';
$language['ru']['year_to_date'] = 'С начала года';
$language['ru']['all'] = 'Все';
$language['ru']['subscribers'] = 'Подписчики';
$language['ru']['monthly_last_3_months'] = 'Ежемесячно за последние 3 месяца';
$language['ru']['new_signup_yesterday'] = 'Письма новых подписчиков вчера:';
$language['ru']['email'] = 'Email';
$language['ru']['no_email_sent_yesterday'] = 'ВЧЕРА НЕТ ОТПРАВЛЕННЫХ ПИСЕМ С ДНЕМ РОЖДЕНИЯ';
$language['ru']['email_sent_yesterday'] = 'ОТПРАВЛЕННЫЕ ВЧЕРА ПИСЬМА С ДНЕМ РОЖДЕНИЯ';
$language['ru']['subscriber_email'] = 'Почта подписчика';
$language['ru']['birthday'] = 'День рождения';
$language['ru']['sent_on'] = 'Отправлено';
$language['ru']['email_subject'] = 'Дневной отчет для ';
$language['ru']['email_body'] = 'ЭТО ПИСЬМО СГЕНЕРИРОВАНО АВТОМАТИЧЕСКИ. ПОЖАЛУЙСТА, НЕ ОТВЕЧАЙТЕ НА НЕГО. Для получения помощи обратитесь по адресу support@Riiwards.com';
$language['ru']['bday_reward_sent'] = 'Отправлено наград с днем рождения';
$language['en']['congrats_msg'] = "Congratulations! You have |total_cust| signups for your Riiwards Birthday Club. ";
$language['en']['subscriber_limit_msg'] = "Please note that you will need to upgrade to Premium plan when you reach |subscriber_limit| signups.";

