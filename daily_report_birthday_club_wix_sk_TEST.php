<?php
/* ----------------------------
Daily report sent to wix merchants for birthday club

10	2	*	*	*	 /usr/bin/curl   http://dev.riiwards.com/dir/crons/daily_report_birthday_club.php

-------------------------------  */

define('DOC_ROOT', dirname(__FILE__));
include_once DOC_ROOT . '/lib/system.php';

$day = 24 * 60 * 60;
$today_time = time();
$yesterdayTime = $today_time - $day;

//$yesterday = date('Y-m-d', $yesterdayTime);
//$yesterday = date('M j, Y', $yesterdayTime);


$today = date('Y-m-d');

$today_ = date('M j, Y');

$yesterdayTimeShow = date('M d, Y', $yesterdayTime);

/////////////////////////
// SETTINGS
$notice_limit = '3';    // ?? set this to 20 after testing


// GET WIX MERCHANTS WITH BIRTHDAY CLUB WHO HAVE VERIFIED THEIR EMAIL

// ?? remove email and limit after testing

$sql = " select a.mem_id, a.email, b.clb_id, b.name, b.organization, b.folder, a.source, b.subscriber_limit
from members a, clubs b
where a.mem_id = b.mem_id
and a.source = 'wix'
and (a.email not like '%@i680.com' and a.email not like '%@riiwards.com')
and b.daily_rep = '1'
and a.verified = '1'
order by b.name  ;
  ";

$merchants = $GLOBALS ['DB']->result($sql);

// echo $sql;
// echo "<br/>";

//  echo "<pre>";
//  print_r($merchants);
//  echo "<pre>";

// die('DEBUG1');

$cnt = 0;

foreach ($merchants as $mer) {

    $cnt++;

    $clb_id = $mer['clb_id'];
    $mem_id = $mer['mem_id'];
    $folder = $mer['folder'];
    $mrchnt_name = $mer['name'];
    $mrchnt_email = $mer['email'];
    $organization = $mer['organization'];
    $subscriber_limit = $mer['subscriber_limit'];
    
    $locale = $mer['locale'];

    if (!$locale) $locale = "En";

        $langIndex = strtolower($locale);
    echo "$cnt clb_id $clb_id mem_id $mem_id folder $folder mrchnt_name $mrchnt_name mrchnt_email $mrchnt_email organization $organization subscriber_limit $subscriber_limit<br/>";
    
    $yesterday = _m($langIndex, strtolower(date('M', $yesterdayTime)));
    $yesterday .= ' ';
    $yesterday .= _i($langIndex, date('j, Y', $yesterdayTime));

    // die('DEBUG1');

    $report_msg = "<hr/>";

    $report_msg .= '<b>' .  _t($langIndex, 'bdc_daily_rep','daily_report') .$organization . ' - '.$yesterday .'<br></b>';

// ---------------------------------
//  collect all data
// ---------------------------------

// ---------------------------------
//  Subscribers
// ---------------------------------
// TOTAL

    $sql = "  select count(*)
    from member_alertees
    where mem_id = '$mem_id '
                   ";
    $total_cust = $GLOBALS ['DB']->single($sql);

    // skip if there is no subscriber

    if (!$total_cust || $total_cust < '1') {

        echo " $cnt clb_id $clb_id total_cust $total_cust has no subscriber, skipping ....<br/>";
        continue;
    } else {

        echo "$cnt clb_id $clb_id total_cust $total_cust<br/>";

    }


// YTD

    $sql = "  select count(*)
    from members_clubs
    where year(created) = year(now())
    and clb_id = '$clb_id '
                   ";
    $ytd_cust = $GLOBALS ['DB']->single($sql);

    // skip if there is no subscriber

    if (!$ytd_cust || $ytd_cust < '1') {

        echo " $cnt clb_id $clb_id ytd_cust $ytd_cust has no subscriber, skipping ....<br/>";
        continue;
    } else {

        echo "$cnt clb_id $clb_id ytd_cust $ytd_cust<br/>";

    }


// NEW

    $sql = "  select count(*)
    from members_clubs
    where clb_id = '$clb_id '
    and date(created) = date(now()) - interval 1 day ;";

    $new_cust = $GLOBALS ['DB']->single($sql);

    // skip if there is no new subscriber

    if (!$new_cust || $new_cust < '1') {

        echo " $cnt clb_id $clb_id has no new subscriber, skipping ....<br/>";
        // continue;
    }else {

        echo "$cnt clb_id $clb_id new_cust $new_cust<br/>";

    }

// MONTHLY FOR LAST 3 MONTHS

$sql = "select month(now());";

$mnth = $GLOBALS ['DB']->single($sql);

$sql = "select year(now());";
$year = $GLOBALS ['DB']->single($sql);


$sql = "select month(now() - interval 1 month); ";

$mnth_1 = $GLOBALS ['DB']->single($sql);

$sql = "select year(now() - interval 1 month);";
$year_1 = $GLOBALS ['DB']->single($sql);

$sql = "SELECT left(MONTHNAME(STR_TO_DATE($mnth_1, '%m')),3) from dual ;";
$mnth_1_n = $GLOBALS ['DB']->single($sql);

$sql = "select month(now() - interval 2 month); ";

$mnth_2 = $GLOBALS ['DB']->single($sql);

$sql = "select year(now() - interval 2 month);";
$year_2 = $GLOBALS ['DB']->single($sql);

$sql = "SELECT left(MONTHNAME(STR_TO_DATE($mnth_2, '%m')),3) from dual ;";
$mnth_2_n = $GLOBALS ['DB']->single($sql);


$sql = "select month(now() - interval 3 month); ";

$mnth_3 = $GLOBALS ['DB']->single($sql);
$sql = "SELECT left(MONTHNAME(STR_TO_DATE($mnth_3, '%m')),3) from dual ;";
$mnth_3_n = $GLOBALS ['DB']->single($sql);
$sql = "select year(now() - interval 3 month);";
$year_3 = $GLOBALS ['DB']->single($sql);

echo "$mnth_1 $mnth_1_n $year_1  $mnth_2 $mnth_2_n $year_2 $mnth_3 $mnth_3_n $year_3 <br/>";


    $sql = "  select count(*)
    from members_clubs
    where clb_id = '$clb_id '
    and year(created) = '$year_1'
    and month(created) = '$mnth_1'";

    $cust_mnth_1 = $GLOBALS ['DB']->single($sql);

    $sql = "  select count(*)
    from members_clubs
    where clb_id = '$clb_id '
    and year(created) = '$year_2'
    and month(created) = '$mnth_2'";

    $cust_mnth_2 = $GLOBALS ['DB']->single($sql);

    $sql = "  select count(*)
    from members_clubs
    where clb_id = '$clb_id '
    and year(created) = '$year_3'
    and month(created) = '$mnth_3'";

    $cust_mnth_3 = $GLOBALS ['DB']->single($sql);

// ---------------------------------
//  Birthday rewards
// ---------------------------------

//  yesterday

$sql = "select count(*)
		from vip_reward_emails_log a, clubs b
		where a.reward_clb_id = b.clb_id
	    and a.reward_clb_id = '$clb_id'
		and a.reward_type = 'BD'
		and date(a.sent_time) = date(now()) - interval 1 day
        ;";

$cnt_bday_yest = $GLOBALS ['DB']->single($sql);

//  ytd

$sql = "select count(*)
		from vip_reward_emails_log a, clubs b
		where a.reward_clb_id = b.clb_id
	    and a.reward_clb_id = '$clb_id'
		and a.reward_type = 'BD'
		and year(a.sent_time) = year(now())
        ;";

$cnt_bday_ytd = $GLOBALS ['DB']->single($sql);

// all

$sql = "select count(*)
		from vip_reward_emails_log a, clubs b
		where a.reward_clb_id = b.clb_id
	    and a.reward_clb_id = '$clb_id'
		and a.reward_type = 'BD'
        ;";

$cnt_bday = $GLOBALS ['DB']->single($sql);

// monthly for last 3 months

$sql = "select count(*)
		from vip_reward_emails_log a, clubs b
		where a.reward_clb_id = b.clb_id
	    and a.reward_clb_id = '$clb_id'
		and a.reward_type = 'BD'
		and year(sent_time) = '$year_1'
		and month(sent_time) = '$mnth_1'";

$cnt_bday_mnth_1 = $GLOBALS ['DB']->single($sql);

$sql = "select count(*)
		from vip_reward_emails_log a, clubs b
		where a.reward_clb_id = b.clb_id
	    and a.reward_clb_id = '$clb_id'
		and a.reward_type = 'BD'
		and year(sent_time) = '$year_2'
		and month(sent_time) = '$mnth_2'";

$cnt_bday_mnth_2 = $GLOBALS ['DB']->single($sql);

$sql = "select count(*)
		from vip_reward_emails_log a, clubs b
		where a.reward_clb_id = b.clb_id
	    and a.reward_clb_id = '$clb_id'
		and a.reward_type = 'BD'
		and year(sent_time) = '$year_3'
		and month(sent_time) = '$mnth_3'";

$cnt_bday_mnth_3 = $GLOBALS ['DB']->single($sql);


// ---------------------------
//   report format
// ---------------------------


        if ($total_cust >= $notice_limit && $total_cust < $subscriber_limit)
        {
         $report_msg .= "<hr/>";
         
         $congratsMsg = _t($langIndex, 'bdc_daily_rep','congrats_msg', array('total_cust' => $total_cust));
         $congratsMsg .= '<br />';
         $congratsMsg .= _t($langIndex, 'bdc_daily_rep','subscriber_limit_msg', array('subscriber_limit' => $subscriber_limit));

        $report_msg .= "<span><b>$congratsMsg</b></span>";

        $report_msg .= "<hr/><hr/>";


        }

        $report_msg .= "<b>" . _t($langIndex, 'bdc_daily_rep','summary') . "</b>";
        $report_msg .= "<br/>";

        $report_msg .= "<table border='1' cellpadding='1' cellspacing='1'>
        <tr>
        <th>&nbsp;</th>
        <th>" . _t($langIndex, 'bdc_daily_rep','yesterday') . "</th>
        <th>" . _t($langIndex, 'bdc_daily_rep','year_to_date')  . "</th>
        <th>" . _t($langIndex, 'bdc_daily_rep','all')  . "</th>

         </tr>";

            $report_msg .= "<tr>";
            $report_msg .= "<td width=200>" . "&nbsp;  " . _t($langIndex, 'bdc_daily_rep','subscribers') . "</td>";
            $report_msg .= "<td style='width:100;text-align:center'>" .  "&nbsp;"  . $new_cust . "</td>";
            $report_msg .= "<td style='width:100;text-align:center'>" .  "&nbsp;"  . $ytd_cust . "</td>";
            $report_msg .= "<td style='width:100;text-align:center'>" .  "&nbsp;"  . $total_cust . "</td>";
            $report_msg .= "</tr>";

            $report_msg .= "<tr>";
            $report_msg .= "<td>" . "&nbsp;  " .  _t($langIndex, 'bdc_daily_rep','bday_reward_sent') . "</td>";
            $report_msg .= "<td style='width:100;text-align:center'>" .  "&nbsp;"  . $cnt_bday_yest . "</td>";
            $report_msg .= "<td style='width:100;text-align:center'>" .  "&nbsp;"  . $cnt_bday_ytd . "</td>";
            $report_msg .= "<td style='width:100;text-align:center'>" .  "&nbsp;"  . $cnt_bday . "</td>";
            $report_msg .= "</tr>";


        $report_msg .= "</table><br/>";

        $report_msg .= "<b>" . _t($langIndex, 'bdc_daily_rep','monthly_last_3_months') . "</b>";
        $report_msg .= "<br/>";

        $report_msg .= "<table border='1' cellpadding='1' cellspacing='1'>
        <tr>
        <th>&nbsp;</th>
        <th>" . _m($langIndex, strtolower($mnth_1_n)) . "</th>
        <th>" . _m($langIndex, strtolower($mnth_2_n)) . "</th>
        <th>" . _m($langIndex, strtolower($mnth_3_n)) . "</th>

         </tr>";

            $report_msg .= "<tr>";
            $report_msg .= "<td width=200>" . "&nbsp;  " . _t($langIndex, 'bdc_daily_rep','subscribers') . "</td>";
            $report_msg .= "<td style='width:100;text-align:center'>" .  "&nbsp;"  . _i($langIndex, $cust_mnth_1) . "</td>";
            $report_msg .= "<td style='width:100;text-align:center'>" .  "&nbsp;"  . _i($langIndex, $cust_mnth_2) . "</td>";
            $report_msg .= "<td style='width:100;text-align:center'>" .  "&nbsp;"  . _i($langIndex, $cust_mnth_3) . "</td>";
            $report_msg .= "</tr>";

            $report_msg .= "<tr>";
            $report_msg .= "<td>" . "&nbsp;  " .  _t($langIndex, 'bdc_daily_rep','bday_reward_sent') . "</td>";
            $report_msg .= "<td style='width:100;text-align:center'>" .  "&nbsp;"  . _i($langIndex, $cnt_bday_mnth_1) . "</td>";
            $report_msg .= "<td style='width:100;text-align:center'>" .  "&nbsp;"  . _i($langIndex, $cnt_bday_mnth_2) . "</td>";
            $report_msg .= "<td style='width:100;text-align:center'>" .  "&nbsp;"  . _i($langIndex, $cnt_bday_mnth_3) . "</td>";
            $report_msg .= "</tr>";


        $report_msg .= "</table><br/>";

// ///////////////////////////////////////
// DETAILS OF SIGNUPS YESTERDAY

    $sql = "select b.email
            from members_clubs a, members b
			where a.mem_id = b.mem_id
			and date(a.created) = date(now()) - interval 1 day
			and a.clb_id = '$clb_id' ";

    $signup_detail = $GLOBALS ['DB']->result($sql);
    $new_signup = count($signup_detail);

    if ($new_signup > 0) {

        $report_msg .= "<b>" . _t($langIndex, 'bdc_daily_rep','new_signup_yesterday') . "</b>" . ' '.$new_signup;

        $report_msg .= "<table border='1' cellpadding='1' cellspacing='1'>
        <tr>
        <th>". _t($langIndex, 'bdc_daily_rep','email') ."</th>
         </tr>";
        foreach ($signup_detail as $r) {
            $report_msg .= "<tr>";
            $report_msg .= "<td width=300>" . $r['email'] . "</td>";
            $report_msg .= "</tr>";
        }


        $report_msg .= "</table><br/>";
    }


// ///////////////////////////////////////
// BIRTHDAY EMAILS SENT YESTERDAY
// ///////////////////////////////////////

$sql = "select a.sent_to_email , a.event_date, b.name, a.sent_time
		from vip_reward_emails_log a, clubs b
		where a.reward_clb_id = b.clb_id
	    and a.reward_clb_id = '$clb_id'
		and a.reward_type = 'BD'
		and date(a.sent_time) > date(now()) - interval 1 day
		order by a.sent_to_email, b.name
        ;";

$result = $GLOBALS ['DB']->result($sql);
$count = count($result);

if ($count == 0) {

	   // $report_msg .= "<br/>";
	   $report_msg .= "<hr/>";
	   $report_msg .= _t($langIndex, 'bdc_daily_rep','no_email_sent_yesterday') . "<Br/>";
	   $report_msg .= "<hr/>";
	   //$report_msg .= "<br/>";

} else {

	   // $report_msg .= "<br/>";
	   $report_msg .= "<hr/>";
	   $report_msg .= _t($langIndex, 'bdc_daily_rep','email_sent_yesterday') . ":&nbsp;$count<Br/>";
	   $report_msg .= "<hr/>";

	   $report_msg .= "<table border=1>
	                       <tr><th>" . _t($langIndex, 'bdc_daily_rep','subscriber_email') . "</th><th>" . _t($langIndex, 'bdc_daily_rep','birthday') . "</th><th>" . _t($langIndex, 'bdc_daily_rep','sent_on') . "</th>
	                       </tr>";

		foreach ($result as $m)
		{
			$email = $m['sent_to_email'];
			$birthday = $m['event_date'];
			$sent_time = $m['sent_time'];

			// convert birthday to mmm nn format

            $birthday_f = date('M j', strtotime($birthday));

            $report_msg .= "<tr>";

            $report_msg .= "<td style='width:250;text-align:left'>" .  "&nbsp;"  .
            $email . "</td>";
            $report_msg .= "<td style='width:120;text-align:left'>" .  "&nbsp;"  . $birthday_f . "</td>";
            $report_msg .= "<td style='width:160;text-align:left'>" .  "&nbsp;"  . $sent_time . "</td>";
            $report_msg .= "</tr>";



		}
		$report_msg .= "</table>";


}

echo  $report_msg;
echo '<br/>';



    ////////////////////get mails////////////////


//    $mrchnt_email .= ", support@riiwards.com";    // ?? FOR MONITORING
    
    // ?? ADD EMAIL FOR TESTING

    // $mrchnt_email = "uday@riiwards.com";

    echo "emails to send to $mrchnt_email <br>";

    $organization= html_entity_decode($organization);
    $sender_name = "Riiwards Birthday Club";
    $sender_email = 'DoNotReply@Riiwards.com';

    $subject = _t($langIndex, 'bdc_daily_rep','email_subject') . "$yesterdayTimeShow";


    $body = _t($langIndex, 'bdc_daily_rep','email_body') . "<br/><br/>";
    $body .= $report_msg;

    $mail_details = array(
        "recipient_name" => $mrchnt_email,
        "recipient" => $mrchnt_email,
        "recipient_email" => $mrchnt_email,

        "sender_name" => $sender_name,
        "sender_email" => $sender_email,
        'bcc_mails' => array('support@riiwards.com'),

        "subject" => $subject,
        "body" => $body
    );

//    include_once(DOC_ROOT . "/classes/Mail.class.php");
    $GLOBALS ["Mail"]->details = $mail_details;

    // ?? uncomment for live

    $GLOBALS ["Mail"]->instant();

    sleep(1);


    echo 'sent mail to:' . $mrchnt_email;
    //    echo '<hr/>';
    echo '<hr/>';
    echo '<br/>';
    //    die;
}


echo 'JOB COMPLETED';


?>

<script type="text/javascript" src="/dir/themes/handshakes_plain/javascript/jquery-1.3.2.min.js"></script>

<script language="javascript">
    var months = new Array(12);
    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";

    $(document).ready(function () {
        $('.tz').each(function (k, v) {

            timeStr = $(v).html();
            timeStr = $.trim(timeStr);


            if (timeStr == '') {
                return true;
            }

            arr = timeStr.split(" ");
            timeShow = arr[0];

//            daymonthyear = arr[1];
//            daymonthyear_arr = daymonthyear.split('-');

            // alert("daymonthyear_arr is:" + daymonthyear_arr[0] + " " + daymonthyear_arr[1] + " " + daymonthyear_arr[2]);

            // alert("timeStr is:" + timeStr + " " + "daymonthyear is:"+  daymonthyear + " " + "timeShow is:" + timeShow );


//            month_ = daymonthyear_arr[1];

            var dateNow=new Date();

            monthIndex = dateNow.getMonth();

            month = months[monthIndex];

            // alert("month_  is:" + month_ + "monthIndex is:" + monthIndex + "month is:" + month);
            var timeStr = month + " " + dateNow.getDate() + "," + dateNow.getFullYear() + " " + timeShow;


            var myDate = new Date(timeStr);


            localtime = convertDateToLocalTime(myDate)

            localtimeDate = new Date(localtime);
            var endYear = localtimeDate.getFullYear();
            endYear = endYear.toString().substring(2);
            var endMonth = localtimeDate.getMonth();
            var endDay = localtimeDate.getDate();
            var endHour = localtimeDate.getHours();
            var endMin = localtimeDate.getMinutes();
//            var endYear = localtimeDate.getYear() + 1900;

            ampm = "am";
            if (endHour < 12) {
                ampm = "am";
            }

            if (endHour >= 12) {
                ampm = "pm";
                endHour = endHour - 12;
            }
            if (endMin < 10) {
                endMin = "0" + endMin;
            }

            monthIndex = parseInt(endMonth);
            month = months[monthIndex];

//            timeStr = month + " " + endDay + "," + endYear + " " + endHour + ":" + endMin + " " + ampm;
            timeStr = endDay + " " + month + ", " + endHour + ":" + endMin + " " + ampm;
//            timeStr =  endHour + ":" + endMin + " " + ampm;

            //        $(v).html( $(v).html()+" "+timeStr);
            $(v).html(timeStr);

        });

    });

    function convertDateToLocalTime(myDate) {
        // nualertTimeZone = 7*60 //todo:

        // //////////////////////////////////////////   TO DO  ????
        localTimeZone = 6 * 60 // ???? get dynamically for each client

        var localDate = new Date();

        utctime = myDate.getTime() + (localTimeZone * 60000);
        localtime = utctime - (localDate.getTimezoneOffset() * 60000);

        return localtime;
    }

</script>
