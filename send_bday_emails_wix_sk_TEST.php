<h1>Processing Birthday Rewards</h1>

<style>
    .warn {
        margin: 5px;
        background: #FFFF77;
        border: 1px solid green;
    }

    .error {
        font-size: 12pt;
        font-weight: bold;
        color: red;
    }

</style>

<?php
/* --------------------------
Udhe Ahluwalia
21 Dec 2014
Send birthday emails for Wix

dev.riiwards.com/dir/crons/send_bday_emails_wix.php

Uday 30 may15 added localization

----------------------------- */
// ///////////////////////////
// DOC_ROOT

// WWW

//include_once("/var/www/vhosts/riiwards.com/httpdocs/dir/includes/config/config.inc.php");
//define("LOG_ROOT", "/var/www/vhosts/riiwards.com/httpdocs/dir/crons/log/reward_emails/");


// DEV

include_once("/var/www/vhosts/dev.riiwards.com/httpdocs/dir/includes/config/config.inc.php");
define("LOG_ROOT", "/var/www/vhosts/dev.riiwards.com/httpdocs/dir/crons/log/reward_emails/");

include_once(DOC_ROOT . "/libraries.php");
include_once(DOC_ROOT . "/classes/Clubs.class.php");

logMsg("DOC_ROOT is " . DOC_ROOT);
logMsg("LOG_ROOT is " . LOG_ROOT);

///////////////////////////////////////////
// LOG FILE
global $f, $logfile ;

$logfile = LOG_ROOT . 'send_bday_emails_wix' . date('Y_m_d') . "_TEST.log";

$f = fopen($logfile, 'a');

logMsg("Log file is $logfile<br/>");

// ///////////////////////////
// INIT

ini_set("display_errors", "ON");
error_reporting(E_ALL & ~E_NOTICE);
// set_time_limit(60);


// ///////////////////////////
// SETTINGS
// ///////////////////////////

$data_check_only = '0'; // ?? 0 will do full run, 1 will only read data
// 2 will check data

$send_emails = '1'; // ?? 1 to send emails; 0 to not send

if ($data_check_only == '1') {
    logMsg("DATA READ ONLY RUN <br/>");
} elseif ($data_check_only == '2') {
    logMsg("DATA CHECK ONLY RUN <br/>");
} elseif ($data_check_only == '0') {
    logMsg("DATA FULL RUN<br/>");
} else {
    logMsg("ERROR: WHICH RUN IS THIS?<br/><br/>");
    die();
}

if ($send_emails == '1') {
    logMsg("EMAILS WILL BE SENT <br/>");
} else {
    logMsg("EMAILS WILL NOT BE SENT <br/>");
}

// ///////////////////////////
// LIMITS

$max_cnt = 0; //  ?? SET FOR TESTING, O FOR NO LIMIT
$cnt = 0;
$$cnt_merchant = 0;
$cnt_sent_before = 0;
$cnt_sent_now = 0;


///////////////////////////////////////////
//  GET EVENT TYPE

// $event_type=$GLOBALS["Get"]->val("event");

$event_type = 'BD';

$event_type_txt = "Birthday";

if (!$event_type) {
    logMsg("<span class='error'>10 ERROR: Please enter event type and try again.</span><br>");
    exit;

}
///////////////////////////////////////////
//  FIND WIX MERCHANTS WHO HAVE A BIRTHDAY REWARD

// ?? remove clb_id and limit after testing

$sql = "
  select a.mem_id,a.fname, a.lname, b.clb_id, d.locale, b.folder, a.email, b.name, c.birthday_rwd
        from members a, clubs b, clubs_rewards c, clubs_authorization d
        where a.mem_id = b.mem_id
        and b.clb_id = c.clb_id
        and c.clb_id = d.clb_id
        and c.clb_id = '1122179'
        and a.source = 'WIX'
        and (c.birthday_rwd is not null and length(trim(c.birthday_rwd)) > 3)
        -- and a.verified = '1'
        and c.active = '1'
        and b.active = '1'
        and ( a.email LIKE 'uday@riiwards.com')
        order by b.clb_id
        ";

$merchants = $GLOBALS ['DB']->result($sql);

// echo $sql;

//echo "<br/>";

echo "<pre>";
print_r($merchants);
echo "<pre>";

$count = count($merchants);

echo "count is $count<br/>";

if ($count < 1) {
    logMsg("<span class=\"error\"> No Wix merchant to process, EXITING..</span><br>");
    exit;
}

// die('DEBUG 1');

foreach ($merchants as $mer)
{
    $cnt_merchant++;

    $clb_id = $mer['clb_id'];
    $name = $mer['name'];
    //$locale = $mer['locale'];

    //if (!$locale) $locale = "En";
    $locales = array('de', 'en', 'es', 'pt', 'tr');
    
    foreach ($locales as $locale) {

    logMsg("Processing merchant $cnt_merchant clb_id $clb_id name $name.locale $locale.</span><br>");
    
    ///////////////////////////////////////////
//  GET EVENT TEMPLATES

$file_prefix = './langs/' . strtolower($locale) . '/nuvip_email_reward_wix_';

$reward_email_txt = $file_prefix . $event_type . ".txt";

$reward_email_html = $file_prefix . $event_type . ".html";

$reward_email_ini = $file_prefix . $event_type . ".ini";

logMsg("Reward templates are : <br/>$reward_email_txt
$reward_email_html<br/>$reward_email_ini<br/>");

///////////////////////////////////////////
//  CHECK FILES EXIST

if (!file_exists($reward_email_txt)) {
    logMsg("<span class='error'>14 ERROR: Template file $reward_email_txt NOT FOUND... Exiting..</span><br>");
    exit;
}
if (!file_exists($reward_email_html)) {
    logMsg("<span class='error'>15 ERROR: Template file $reward_email_html NOT FOUND... Exiting..</span><br>");
    exit;
}
if (!file_exists($reward_email_ini)) {
    logMsg("<span class='error'>16 ERROR: Template file $reward_email_ini NOT FOUND... Exiting..</span><br>");
    exit;
}

///////////////////////////////////////////
//  FIND EVENT IN NEXT 7 DAYS
//  TO DO - IF BDAY WITHIN 7 DAYS END OF YEAR

// ?? COMMENT OUT TEST email, CLB_ID & LIMIT

$sql = "select distinct a.mem_id , a.email, a.fname, a.lname, b.birth_month,  m.month as birth_month_text, b.birth_day, e.clb_id, e.name as mrcht_name, e.folder, e.mem_id as mrcht_mem_id, g.email as mrcht_email, trim(h.birthday_rwd) as birthday_rwd, trim(h.rwd_code) as rwd_code

from members a, profiles b, months m, member_alertees d, clubs e, member_lists f, members g, clubs_rewards h

where a.mem_id = b.mem_id
and b.birth_month = m.mnth_id
and a.mem_id = d.alertee_mem_id
and d.mem_id = e.mem_id
and d.mem_id = f.mem_id
and d.mem_id = g.mem_id
and e.clb_id = h.clb_id
and d.list_id = f.list_id
and f.list_name = 'VIPs'
and e.vip_merchant = '1'
and a.vip = '1'
and a.verified = '1'
and h.active = '1'
and e.active = '1'
and b.birth_month > 0
and b.birth_day > 0
and ( a.email not like '%@i680.com' and a.email not like '%@riiwards.com' and a.email not like 'CBake%')
and e.clb_id = '$clb_id'
and length(trim(h.birthday_rwd)) > 1
and (

        (  concat(year(now()),'-',b.birth_month,'-',b.birth_day) > current_date() - interval 7 day
        and
        concat(year(now()),'-',b.birth_month,'-',b.birth_day) <= current_date()+ interval 7 day
        )

        or

        ( concat(year(now())+1,'-',b.birth_month,'-',b.birth_day) > current_date() - interval 7 day
        and
        concat(year(now())+1,'-',b.birth_month,'-',b.birth_day) <= current_date()+ interval 7 day
        )
)
order by a.email;";


$result = $GLOBALS ['DB']->result($sql);

//echo $sql;

//echo "<br/>";

$count = count($result);

//echo "<pre>";
//print_r($result);
//echo "<pre>";

if ($count == 0) {
    logMsg("<span class=\"error\"> No event type $event_type $event_type_txt to process, EXITING..</span><br>");
    exit;
} else {
    logMsg("PROCESSING $count emails for event type $event_type  $event_type_txt<br>");

    if ($data_check_only == '1') {
        foreach ($result as $res)
        {
            $cnt++;

            $vip_email = $res["email"];
            $folder = $res["folder"];

            echo "$cnt VIP Email $vip_email Member Link $folder<br/>";
        }

        echo "<br/>";
        exit;
    }
}

// die('DEBUG 2');


///////////////////////////////////////////
//  PROCESS EACH MEMBER

$cnt = 0;
$rwd_code = null;

foreach ($result as $res)
{
    $cnt++;

    if ($max_cnt > 0 && $cnt > $max_cnt) {
        logMsg("<br/>Max count $max_cnt exceeded.. EXITING <br> <br>");
        exit;
    }

	if ($cnt % 100 == 0)
	{
	  logMsg("<b>Processed 100 emails, taking a nap.</b>");
	  sleep(5);
	} else { sleep (1) ;}

	$vip_email = $res["email"];

    // ?? comment after testing

    // $vip_email = 'uday@riiwards.com' ;
    // $vip_email = 'andy.bush@riiwards.com' ;

    $vip_mem_id = $res["mem_id"];
    $fname = $res["fname"];
    $lname = $res["lname"];

    $birth_month = $res["birth_month"];
    $birth_month_text = $res["birth_month_text"];
    $birth_day = $res["birth_day"];
    $state = $res["state"];
    $city = $res["city"];
    $postal_code = $res["postal_code"];

    $clb_id = $res["clb_id"];
    $mrcht_name = $res["mrcht_name"];
    $folder = $res["folder"];
    $mrcht_mem_id = $res["mrcht_mem_id"];
    $mrcht_email = $res["mrcht_email"];
    $mrcht_zip = $res["mrcht_zip"];
    $birthday_reward = $res["birthday_rwd"];
    $anniversary_reward = $res["anniversary_rwd"];

    $rwd_code = $res["rwd_code"];

    // set merchant email to donot reply
    $mrcht_email = 'DoNotReply@riiwards.com';


    if ($event_type == 'BD') {
        logMsg("Processing VIP # $cnt email $vip_email mem_id $vip_mem_id birthday  $birth_month_text $birth_day");

        $reward_text = $birthday_reward;
    }
    elseif ($event_type == 'AD')
    {
        logMsg("Processing VIP # $cnt vip_email $vip_email vip_mem_id $vip_mem_id anniv_month  $anniv_month anniv_month_txt $birth_month_text anniv_day $anniv_day");

        $reward_text = $anniversary_reward;

    }


    // CHECK IF EMAIL ALREADY SENT IN LAST 30 DAYS

    $sql = "
             select *
             from vip_reward_emails_log
             where reward_type = '$event_type'
             and sent_to_email = '$vip_email'
             and reward_clb_id = '$clb_id'
             and date(sent_time) > date(now()) - interval 30 day
             limit 1

              ;";

    $result = $GLOBALS['DB']->row($sql);

    if ($result) {

        $cnt_sent_before++;

        $id = $result["id"];
        $sent_time = $result["sent_time"];

        logMsg("<span class='error'>&nbsp;&nbsp;Email $vip_email Merchant $mrcht_name sent email for $event_type $event_type_txt mail id $id on $sent_time</span>");
        continue;

    } else {
        // logMsg("&nbsp;&nbsp; Email $vip_email Merchant $mrcht_name event_type $event_type_txt email should be sent");
    }

    $mail_details = array();

    if ($event_type == 'BD') {
        if (strlen(trim($birthday_reward)) > 0) {
            // logMsg("&nbsp;&nbsp; $event_type_txt reward reward is:<br>&nbsp;&nbsp; $birthday_reward");
            $mail_details["reward_text"] = $birthday_reward;
        } else

        {
            logMsg("<span class='error'> FATAL ERROR:$cnt Birthday reward not found for clb_id $clb_id..SKIPPING</span><br>");
            continue;

        }
    }


    // CREATE VALUES FOR MACROS IN TEMPLATES

    $club = $GLOBALS["Clubs"]->getClubDetail($clb_id);

    $GLOBALS["smarty"]->assign("club", $club);


    //  EMAIL TEMPLATE
    $type = $event_type;

    $mail_details['_subtemplates'] = array();


    if (!$headers = parse_ini_file($reward_email_ini)
        OR !$mail_details["body"] = file_get_contents($reward_email_txt)
        OR empty($headers["subject"])
    ) {
        // echo __FILE__ . __LINE__ . '<BR/>';
        echo $reward_email_ini . '<BR/>';

        return false;
    }

    // VIP EMAIL
    $customer_mem_id = $vip_mem_id;

    // $sql_query = "select email from members where mem_id='$customer_mem_id'";
    // $customer_email = $GLOBALS["DB"]->single($sql_query);

    $customer_email = $vip_email;

    // UNSUBSCRIBE
    $unsubscribe_subject = 'Unsubscribe from Riiwards clb_id ' . $clb_id . '.';
    $GLOBALS["smarty"]->assign("subject", $unsubscribe_subject);

    $unsubscribe_url = URL . '/index.php?page=unsubscribe_club&email={$email_to}&clb_id=' . "{$clb_id}";

    $unsubscribe = $GLOBALS["smarty"]->fetch("unsubscribe_riiwards.tpl");
    //        echo '<br/>UNSUBSCRIBE:' . $unsubscribe . '<BR/><BR/>';

    $unsubscribe = str_replace('{$email_to}', $customer_email, $unsubscribe);

    $mail_details["unsubscribe"] = $unsubscribe;

    $headers['sender_email'] = $mrcht_email;

    $is_reward_mail = false;
    if ($mail_details['reward_text']) {
        $loyalty_reward = file_get_contents($file_prefix . $type . '.html');
        $loyalty_reward = preg_replace('/\r/', '', $loyalty_reward);
        $loyalty_reward = preg_replace('/\n/', '', $loyalty_reward);
        $mail_details['_subtemplates']['birthday_reward'] = $loyalty_reward;
        $mail_details['_subtemplates']['anniversary_reward'] = $loyalty_reward;

        $is_reward_mail = true;
    }


    //        $mail_details["recipient"] = (int)$customer_mem_id;
    $mail_details["recipient_name"] = $fname . " " . $lname;
    $mail_details["recipient"] = $vip_email;
    $mail_details["recipient_email"] = $vip_email;

	if ($fname != 'First') $mail_details["fname"] = $fname;

    $mail_details["subject"] = $headers["subject"];
    unset($headers["subject"]);

    $mail_details["sender"] = $headers;
    $mail_details["sender_name"] = $mrcht_name;

    $mail_details["member_name"] = $mrcht_name;
    $mail_details["member_email"] = $mrcht_email;
    $mail_details["recipient_full_name"] = $fname . " " . $lname;
    $mail_details["recipient_full_name"] = $customer_email;


    if ($rwd_code && strlen(trim($rwd_code)) > 1 ) {

       $mail_details["rwd_code"] = "Use promo code: ". $rwd_code;

    } else {

       $mail_details["rwd_code"] = null;

    }


    $clb = $GLOBALS['Member']->get_clb($mrcht_mem_id);

    // $mail_details["member_club_link"] = build_link("club", $clb['clb_id']);
    // $mail_details["merchant_sign-up_link"] = $mail_details["member_club_link"];

    $mail_details["member_club_link"] = URL . "/index.php?page=nuvip_signup&id=" . $folder;
    $mail_details_member_club_link =  $mail_details["member_club_link"] ;

    $mail_details["merchant_sign-up_link"] = URL . "/index.php?page=nuvip_signup&id=" . $folder;
    $mail_details_merchant_signup_link = $mail_details["merchant_sign-up_link"] ;

    logMsg("mail_details member_club_link is $mail_details_member_club_link <br/>");
    logMsg("mail_details_merchant_signup_link is $mail_details_merchant_signup_link <br/>");

    $mail_details["member_profile_link"] = URL . "/index.php?page=nuvip_custom_edit&mem_id=" . $customer_mem_id;

    $mail_details["site_http_url"] = URL;

    $mail_details["like_url"] = "index.php?handler=members&action=like_reward&val_code=" . urlencode($val_code);

    $mail_details["dislike_url"] = "index.php?handler=members&action=dislike_reward&val_code=" . urlencode($val_code);

    $mail_details["member_org_name"] = $clb['organization'];
    $mail_details["clubs_organization"] = $clb['organization'];
    $mail_details["date_time"] = date("m/d/Y");

    //val code

    // $val_code = $GLOBALS['Clubs']->get_val_code();
    // $val_code = format_val_code($val_code);

    // $mail_details["val_code"] = $val_code;

    $mail_details["birthday_date"] = $birth_month_text . " " . $birth_day;
    $mail_details["anniversary_date"] = $anniv_month_text . " " . $anniv_day;

    // GENERATE EMAIL

    include_once(DOC_ROOT . "/classes/Mail.class.php");

    if ($send_emails == '1') {


        logMsg("&nbsp;&nbsp;&nbsp; VIP $cnt email sent to $vip_email for merchant $mrcht_name");

        $mail_details["like_url"] = "index.php?handler=members&action=like_other_reward&id=$vip_reward_emails_log_id";
        $mail_details["dislike_url"] = "index.php?handler=members&action=dislike_other_reward&id=$vip_reward_emails_log_id";

        $GLOBALS["Mail"]->details = $mail_details;

        // ?? COMMENT FOR TESTING

        $GLOBALS["Mail"]->send_external();
        $cnt_sent_now++;

        logMsg("<span style='font-size:12pt;color:blue'>VIP count $cnt Email $vip_email for Merchant $mrcht_cnt $mrcht_name Email sent...</span>");

      // INSERT IN LOG TABLE

        if ($event_type == 'BD') {

            $event_date = date('Y') . "-" . $birth_month . "-" . $birth_day;
        } elseif ($event_type == 'AD') {

            $event_date = date('Y') . "-" . $anniv_month . "-" . $anniv_day;
        }

        // echo " Event date is $event_date<br>";

        $sql = "insert into vip_reward_emails_log ( sent_to_mem_id, sent_to_email, reward_clb_id, reward_type, reward_text, event_date, sent_from_email, subject,val_code )
		values ('$vip_mem_id','$vip_email','$clb_id','$event_type', '$reward_text','$event_date', '$mrcht_email','$subject','$val_code'  )";


        // ?? COMMENT FOR TESTING

        $vip_reward_emails_log_id = $GLOBALS["DB"]->insert($sql);

        logMsg("&nbsp;&nbsp;&nbsp; VIP # $cnt Merchant $mrcht_name inserted record in vip_reward_emails_log  mem_id $vip_mem_id vip_email $vip_email clb_id $clb_id");


    } else {
        logMsg("<span style='font-size:12pt;color:blue'>VIP count $cnt Email $vip_email for Merchant $mrcht_cnt $mrcht_name Email WILL BE sent.</span>");

    }

}

    }

}
// -----------------------------------

// ----------------------------------------
//  close and cleanup

logMsg("<br> JOB COMPLETED <br><br>");
logMsg("Total Processed: $cnt, Sent Before: $cnt_sent_before, Sent Now: $cnt_sent_now<br>");

fclose($f);

exit;

function logMsg($msg)
{
    global $f, $logfile ;

    $msg = "$msg\n";

    chmod($logfile,0777);
    fwrite($f, $msg);
    echo nl2br(($msg));

}

?>
