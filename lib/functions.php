<?php

if (!function_exists('pr')) {

    function pr($var) {
        echo '<pre>';
        print_r($var);
        echo '</pre>';
    }

}

if (!function_exists('extract_null_value')) {

    function extract_null_value($value) {
        if (!empty($value)) {
            return false;
        }
        $value.= "test";
        if (hs_ereg("^0", $value)) {
            return false;
        }
        return true;
    }

}

if (!function_exists('hs_ereg')) {

    function hs_ereg($var1, $var2, $var3 = null) {

        if (function_exists("mb_ereg")) {
            return mb_ereg($var1, $var2, $var3);
        }
        return ereg($var1, $var2, $var3);
    }

}

if (!function_exists('handle_mail_patterns')) {

    function handle_mail_patterns($text, $values) {
        $patterns = array_keys($values);
        $patterns[] = "site_name";
        $values["site_name"] = $GLOBALS["config"]["site_name"];

        foreach ($patterns as $pat) {
            if ($pat != "link") {
                $text = hs_ereg_replace("\|" . $pat . "\|", $values[$pat], $text);
            } else {
                $text = hs_ereg_replace("\|" . $pat . "\|", "<a href=\"" . $values[$pat] . "\">" . $values[$pat] . "</a>", $text);
            }
        }
        return $text;
    }

}

if (!function_exists('hs_ereg_replace')) {

    function hs_ereg_replace($var1, $var2, $var3) {

        if (function_exists("mb_ereg_replace")) {
            return mb_ereg_replace($var1, $var2, $var3);
        } else {
            return ereg_replace($var1, $var2, $var3);
        }
    }

}


