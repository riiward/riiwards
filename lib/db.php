<?php

/**
 * Description of db
 *
 * @author sifat
 */
class DB {

    public $pdo;
    public $escapeFieldNames = true;
    private $_sth;

    public function __construct() {
        $config = $GLOBALS['config'];
        $this->connect($config['host'], $config['username'], $config['password'], $config['database_name']);
    }

    public function __destruct() {
        $this->close();
    }

    /**
     * Connect to a database
     * @author sifat
     * @param string $host
     * @param string $username
     * @param string $password
     * @param string $db
     * @return void
     */
    public function connect($host, $username, $password, $db) {
        try {
            $this->pdo = new PDO('mysql:host=' . $host . ';dbname=' . $db, $username, $password, array(
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ));
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    /**
     * close current connection
     */
    public function close() {
        $this->pdo = null;
    }

    /**
     * Prepare and executes an statement
     * @author sifat
     * @param string $sql query to execute
     * @param array $where key value pair of where condition or other
     * @return bool
     */
    protected function _prep($sql, $where) {
        $this->_sth = $this->pdo->prepare($sql);

        $whereArr = array();
        foreach ($where as $key => $value) {
            $whereArr[':' . $key] = $value;
        }
        return $this->_sth->execute($whereArr);
    }

    /**
     * Executes a sql statement and returns results in an array
     * @param string $sql sql statement to execute
     * @param array $where
     * @return mixed
     */
    public function result($sql, $where = array()) {
        if ($this->_prep($sql, $where)) {
            return $this->_sth->fetchAll();
        }
        return false;
    }

    /**
     * Executes a sql statement and returns results in a single row
     * @param string $sql sql statement to execute
     * @param array $where
     * @return mixed
     */
    public function row($sql, $where = array()) {
        if ($this->_prep($sql, $where)) {
            return $this->_sth->fetch();
        }
        return false;
    }

    /**
     * Executes a sql statement and returns result of a column
     * @param string $sql sql statement to execute
     * @param array $where
     * @return mixed
     */
    public function single($sql, $where = array()) {
        if ($this->_prep($sql, $where)) {
            return $this->_sth->fetchColumn();
        }
        return false;
    }

    public function lastInsertId() {
        return $this->pdo->lastInsertId();
    }

    /**
     * insert in to table
     * @param string $tableName 
     * @param array $data key value pair of column name and value
     * @return last insert id
     */
    public function insert($tableName, $data) {
        $keys = array_keys($data);

        $sql = "INSERT INTO $tableName (";
        if ($this->escapeFieldNames) {
            $sql .= '`' . implode('`,`', $keys) . '`';
        } else {
            $sql .= implode(',', $keys);
        }
        $sql .= ") VALUES (";
        $sql .= ':' . implode(',:', $keys);
        $sql .= ")";

        $this->_prep($sql, $data);
        return $this->lastInsertId();
    }

    /**
     * 
     * @param sting $tableName
     * @param array $data key value pair of column name and value
     * @param type $where key value pair of column name and value
     * @return bool
     */
    public function update($tableName, $data, $where) {
        $sql = "UPDATE $tableName SET ";
        $total = count($data);
        $i = 0;
        foreach ($data as $key => $value) {
            if ($this->escapeFieldNames) {
                $sql .= '`' . $key . '` = :' . $key;
            } else {
                $sql .= $key . ' = :' . $key;
            }
            if (++$i < $total) {
                $sql .= ',';
            }
        }
        if (!empty($where)) {
            $sql .= " WHERE " . $this->_where($where);
        }

        $conditions = $data + $where;
        return $this->_prep($sql, $conditions);
    }

    /**
     * 
     * @param string $tableName
     * @param array $where key value pair of column name and value
     * @return bool
     */
    public function delete($tableName, $where) {
        $sql = "DELETE FROM $tableName";
        $sql .= " WHERE " . $this->_where($where);
        
        return $this->_prep($sql, $where);
    }

    /**
     * 
     * @param string $tableName
     * @param array $fields
     * @param array $where
     * @return array
     */
    public function get($tableName, $fields = array(), $where = array()) {
        $fieldsStr = '';
        if (empty($fields)) {
            $fieldsStr = '*';
        } else {
            if ($this->escapeFieldNames) {
                $fieldsStr .= '`' . implode('`,`', $fields) . '`';
            } else {
                $fieldsStr .= implode(',', $fields);
            }
        }
        $sql = "SELECT $fieldsStr FROM $tableName";
        
        if (!empty($where)) {
            $sql .= " WHERE " . $this->_where($where);
        }
        $this->_prep($sql, $where);
        return $this->_sth->fetchAll();
    }

    protected function _where($where) {
        $sql = '';
        $whereTotal = count($where);
        $w = 0;
        foreach ($where as $key => $value) {
            if ($this->escapeFieldNames) {
                $sql .= '`' . $key . '` = :' . $key;
            } else {
                $sql .= $key . ' = :' . $key;
            }
            if (++$w < $whereTotal) {
                $sql .= " AND ";
            }
        }
        return $sql;
    }

    public function exec($sql) {
        return $this->pdo->exec($sql);
    }

    public function query($sql) {
        return $this->pdo->query($sql);
    }

}

$DB = new DB();
//print_r($DB->result('SELECT * FROM titles WHERE imdb_id=:imdb_id', array('imdb_id' => 'tt2948634')));
//print_r($DB->single('select month(now());'));
//echo $DB->delete('titles', array('series_id' => 'tt0060028', 'series_title' => 'Star Trek'));
//print_r($DB->get('titles', array('title'), array('series_id' => 'tt0071007')));
