<?php

function _t($langIndex, $fileName, $varName, $args = array()) {
    include DOC_ROOT . "/langs/$langIndex/$fileName.php";
    $var = $language[$langIndex][$varName];
    if (!empty($args)) {
        $newarr = array();
        foreach ($args as $k=>$v) {
            $newarr['|' . $k . '|'] = $v;
        }
        $var = str_replace(array_keys($newarr), array_values($newarr), $var);
    }
    return $var;
}

/**
 * Translate month name
 * @param str $langIndex lowercase two char language code
 * @param str $month 3 char/ full month name
 * @return str
 */
function _m($langIndex, $month) {
    include DOC_ROOT . "/langs/$langIndex/months.php";
    return $language[$langIndex][$month];
}

function _i($langIndex, $number) {
    include DOC_ROOT . "/langs/$langIndex/digits.php";
    $str = '';
    $digits = str_split($number);
    foreach ($digits as $digit) {
        if (!preg_match('/[0-9]/', $digit)) {
            $str .= $digit;
        } else {
            $str .= $language[$langIndex][$digit];
        }
        
    }
    return $str;
}