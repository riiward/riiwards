<?php
if (!defined('DOC_ROOT')) {
    die('DOC_ROOT not defined');
}

$config = parse_ini_file(DOC_ROOT . '/config/config.ini');

include_once DOC_ROOT . '/lib/functions.php';
include_once DOC_ROOT . '/lib/lang_functions.php';
include_once DOC_ROOT . '/lib/db.php';
include_once DOC_ROOT . '/lib/mail.php';