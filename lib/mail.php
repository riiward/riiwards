<?php

include_once DOC_ROOT . "/lib/phpmailer/class.phpmailer.php";

class Mail {

    public $mail;
    public $db;
    public $details = array();

    public function __construct() {
        $this->mail = new PHPMailer();
        $this->db = $GLOBALS['DB'];
    }

    function prepare() {

        include(DOC_ROOT . "/config/messages.inc.php");

        if (extract_null_value($this->details["mes_id"])) {
            $subject = $this->details["subject"];
//            $body = $this->details["body"];
        } else {
            $subject = $messages["subject"][$this->details["mes_id"]];
//            $body = $messages["body"][$this->details["mes_id"]];
            $this->details["body"] = $messages["body"][$this->details["mes_id"]];
        }

        if (is_numeric($this->details["recipient"])) {

            $sql_query = "SELECT email,notifications,fname, lname
                                            FROM members,members_settings
                                            WHERE members.mem_id=members_settings.mem_id
                                            AND members.mem_id=" . $this->details["recipient"];


            $row = $this->db->row($sql_query);
            $this->details["recipient_email"] = (eregi("@", $row["email"]) || $this->details["recipient"] != 100000) ? $row["email"] : $GLOBALS["config"]["system_mail"];
            $this->details["recipient_name"] = $row["fname"];

            $fname = trim($row["fname"]);
            $lname = trim($row["lname"]);

            //  $this->details["recipient_full_name"]  = $row["fname"].' '.$row["lname"];
            if ($fname && $fname != 'First') {
                $this->details["recipient_full_name"] = $fname . ' ' . $lname . ', ';
            } else {
                $this->details["recipient_full_name"] = ' ';
            }


            if (!$this->details["notifications"]) {
                $this->details["notifications"] = $row["notifications"];
            }
        } else {
            $this->details["recipient_email"] = $this->details["recipient"];
            $this->details["notifications"] = 1;
            $this->details["recipient_name"] = $this->details["recipient_name"];
        }

        if ($this->details["sender"] == "Admin") {
            $sql_query = "SELECT email,fname,lname FROM members WHERE member_type='a'";
            $admin = $this->db->row($sql_query);
            $this->details["sender_email"] = (eregi("@", $admin["email"])) ? $admin["email"] : $GLOBALS["config"]["system_mail"];
            $this->details["sender_name"] = $admin["fname"] . " " . $admin["lname"];
        } //if
        elseif ($this->details["sender"] && is_numeric($this->details["sender"])) {

            $sql_query = "SELECT email,fname,lname
                                            FROM members
                                            WHERE mem_id=" . $this->details["sender"];

            $row = $this->db->row($sql_query);
            $this->details["sender_email"] = (eregi("@", $row["email"])) ? $row["email"] : $GLOBALS["config"]["system_mail"];
            $this->details["sender_name"] = $row["fname"] . " " . $row["lname"];
        } elseif (is_array($this->details["sender"]) and isset($this->details["sender"]["sender_email"])) {
            $this->details["sender_email"] = $this->details["sender"]["sender_email"];
            $this->details["sender_name"] = (isset($this->details["sender"]["sender_name"])) ? $this->details["sender"]["sender_name"] : '';

            $this->details["sender"] = '';
        } else {
            $this->details["sender_email"] = $GLOBALS["config"]["system_mail"];
            $this->details["sender_name"] = $GLOBALS["config"]["site_name"];
        }

        if (isset($this->details['_subtemplates']) AND is_array($this->details['_subtemplates'])) {
            $this->_parseMailDetails($this->details['_subtemplates']);
            unset($this->details['_subtemplates']);
        }
        $this->_parseMailDetails($this->details);

        $this->details["sender_name"] = html_entity_decode($this->details["sender_name"]);
        $this->details["sender_email"] = html_entity_decode($this->details["sender_email"]);

        $this->details["subject"] = html_entity_decode($subject);
        $this->details["body"] = nl2br($this->details['body']);
    }

    private function _parseMailDetails($arr) {
        if (empty($arr)) {
            return '';
        }
        
        array_walk($arr, function ($value, $key) {
            if (is_array($value)) {
                return false;
            }
            preg_match_all('/\|(.*?)\|/', $value, $matches);
            if (empty($matches[0])) {
                return false;
            }
            foreach ($matches[0] as $r => $k) {
                $this->details[$key] = str_replace($k, $this->details[$matches[1][$r]], $value);
                $value = $this->details[$key];
            }
        });
    }

    function instant() {

        //filter 6503468871@riiwards.com
        if (preg_match('/\d{10}\d*@riiwards\.com/', $this->details["recipient_email"])) {
            return;
        }

//        $encoding = strtoupper(extract_encoding("html"));
        $encoding = strtoupper('utf-8');

        $this->mail = new PHPMailer;

        //mailer settings
        $this->mail->Mailer = $GLOBALS["config"]["mailer"];
        if ($GLOBALS["config"]["mailer"] == "sendmail") {
            $this->mail->Sendmail = $GLOBALS["config"]["mailsetup"];
        } elseif ($GLOBALS["config"]["mailer"] == "smtp") {
            $smtp = explode("*", $GLOBALS["config"]["mailsetup"]);
            $this->mail->Host = $smtp[0];
            $this->mail->Port = $smtp[1];
            if ($smtp[2]) {
                $this->mail->SMTPAuth = 1;
                $this->mail->Username = $smtp[2];
                $this->mail->Password = $smtp[3];
            }
        }
        $this->mail->AddAddress($this->details["recipient_email"], $this->details["recipient_name"]);
        $this->mail->From = $this->details["sender_email"];
        $this->mail->FromName = $this->details["sender_name"];
        $this->mail->CharSet = $encoding;
        $this->mail->ContentType = "text/html";
        $this->mail->Body = $this->details["body"];
        $this->mail->Subject = $this->details["subject"];
        $this->mail->AddReplyTo = ($this->details["sender_email"]);
        //$this->mail->Sender = $this->details["sender_email"];
        $this->mail->Sender = "mailer@Riiwards.com";

        if (!empty($this->details["cc_mails"]) && count($this->details["cc_mails"]) > 0) {

            foreach ($this->details["cc_mails"] as $k) {
//                print_r($k);
//                $this->mail->AddCC($k);
//                $this->mail->AddCC($k);
                $this->mail->AddBCC($k);
            }
        }

        if (!empty($this->details["bcc_mails"]) && count($this->details["bcc_mails"]) > 0) {
            foreach ($this->details["bcc_mails"] as $k) {
                $this->mail->AddBCC($k);
            }
        }


        $this->mail->send();
    }

    function send_external() {

        $this->prepare();
        if (!empty($this->details["notifications"])) {
            $this->instant();
        }
    }

    function send_mass() {

        $this->prepare();
        if (!empty($this->details["notifications"])) {

//            $sql_query = "INSERT INTO mass_mail(`to`,`subject`,`message`,`headers`)
//						  VALUES (':to:',':subject:',':message:',':headers:')";

            $values["to"] = $this->details["recipient_email"] . "|" . $this->details["recipient_name"];
            $values["subject"] = $this->details["subject"];
            $values["message"] = $this->details["body"];
            $values["headers"] = $this->details["sender_email"] . "|" . $this->details["sender_name"];

            $this->values = $values;
//            $this->execute($sql_query);
            $this->db->insert('mass_mail', $values);
        }
    }

    function send_internal() {

        $mes_id = $this->details["mes_id"];

        if ($mes_id) {
            include(DOC_ROOT . "/config/messages.inc.php");
            $subject = handle_mail_patterns($messages["subject"][$mes_id], $this->details);
            $body = handle_mail_patterns($messages["body"][$mes_id], $this->details);
        } //if
        else {
            $subject = $this->details["subject"];
            $body = $this->details["body"];
        }
        //else
//        $recipient_member_type = $GLOBALS["Member"]->member_type($this->details["recipient"]);
        $recipient_member_type = $this->db->single("SELECT member_type FROM members WHERE mem_id=" . $this->details["recipient"]);

        if (empty($this->details["message_type"])) {
            $this->details["message_type"] = "message";
        }

        if ($recipient_member_type != 'a' || $this->details["message_type"] != "message") {

//            $sql_query = "INSERT INTO internal_mail(mem_id,per_id,subject,message,message_type,special,date,msg_type_id)
//						  VALUES (:mem_id:,:per_id:,':subject:',':message:',':message_type:',':special:',:time:,:msg_type_id:)";

            $values["mem_id"] = $this->details["recipient"];
            $values["per_id"] = $this->details["sender"];
            $values["subject"] = $subject;
            $values["message"] = $body;
            $values["time"] = time();
            $values["special"] = $this->details["special"];
            $values["message_type"] = $this->details["message_type"];

            if (isset($this->details["msg_type_id"])) {
                $values["msg_type_id"] = $this->details["msg_type_id"];
            } else {
                $values["msg_type_id"] = 9;
            }

            $this->values = $values;
//            $this->execute($sql_query);
            $this->db->insert('internal_mail', $values);
        } elseif ($this->details["message_type"] == "message" && $recipient_member_type == 'a') {

//            $sql_query = "INSERT INTO admin_mail (per_id,subject,message,message_type,date)
//						  VALUES (:per_id:,':subject:',':message:',':message_type:',:time:)";

            $values["per_id"] = $this->details["sender"];
            $values["subject"] = $subject;
            $values["message"] = $body;
            $values["time"] = time();
            $values["message_type"] = $this->details["message_type"];

            $this->values = $values;
//            $this->execute($sql_query);
            $this->db->insert('admin_mail', $values);
        }

        return true;
    }

}

$Mail = new Mail();
