<?php

$messages["subject"][0] = "Welcome to |site_name|!";
$messages["body"][0] = "Hi |recipient_name|,

Thank you very much for registering with |site_name|.
We're sure you'll enjoy networking with us and thousands of our members.

Your login information is here consists of
Login: |login|
Password: |password|

To complete your registration, please either click on the following link
|link|
or simply insert this code to the box on a site: |code|

regards,
|site_name| support";

$messages["subject"][1] = "Welcome to |site_name|!";
$messages["body"][1] = "Hi |recipient_name|,

Thank you very much for registering with |site_name|.
We're sure you'll enjoy networking with us and thousands of our members.

Your login information is here consists of
Login: |login|
Password: |password|

regards,
|site_name| support";

$messages["subject"][2] = "|site_name|: Password changed.";
$messages["body"][2] = "Hi |recipient_name|,

You recently changed your password on a site. This email is sent
automatically to inform you about new login details.

Login: |login|
Password: |password|

regards,
|site_name| support";

$messages["subject"][3] = "|site_name|: Email changed.";
$messages["body"][3] = "Hi |recipient_name|,

You recently made a change to your email address.
This email is sent automatically to inform you about new login details.

Login: |login|
Password: |password|

regards,
|site_name| support";

$messages["subject"][4] = "|subject|";
$messages["body"][4] = "|body|

To accept invitation and join |site_name|, please, click on the following link:
|link|";

$messages["subject"][5] = "|site_name|: New email arrived.";
$messages["body"][5] = "Hi |recipient_name|,

New message is waiting for you in your |site_name| inbox.
To read it you need to login to your account and click on a mailbox icon at the header.

regards,
|site_name| support";

$messages["subject"][6] = "|site_name|: Temporary Password";
$messages["body"][6] = "Hi |recipient_name|,

System created a temporary password for you to login and change it to something more memorable.

Login: |login|
Password: |password|

regards,
|site_name| support";

$messages["subject"][7] = "|subject|";
$messages["body"][7] = "|body|

To view an eCard, please, click on the following link:
|link|";

$messages["subject"][8] = "|site_name|: You have received an eCard";
$messages["body"][8] = "Hi |recipient_name|,

You have received an eCard at |site_name|.

To view an eCard, please, click on the following link:
|link|

regards,
|site_name| Support";

$messages["subject"][9] = "|site_name|: Your invitation to join your network has been accepted.";
$messages["body"][9] = "Hi |recipient_name|,

Member |invitee_name| has recently joined |site_name| through your invitation and is now added to your personal network.

regards,
|site_name| Support";

$messages["subject"][10] = "|site_name|: Your eCard has been picked up.";
$messages["body"][10] = "Hi |recipient_name|,

Your eCard has been picked up by a person, identified by email address |person_email|.

regards,
|site_name| Support.";

$messages["subject"][11] = "|site_name|: Your eCard has been picked up.";
$messages["body"][11] = "Hi |recipient_name|,

Your eCard has been picked up by |member_name|.

regards,
|site_name| Support.";

$messages["subject"][12] = "|site_name|: Invitation to visit |member_name|'s Blog";
$messages["body"][12] = "Hi |recipient_name|,

You have received an invitation to visit Blog |blog_name| from |member_name|.
Please, check your mailbox at |site_name|.

regards,
|site_name| Support.";

$messages["subject"][13] = "|subject|";
$messages["body"][13] = "|body|

To visit blog, please, click on the following link:
|link|";

$messages["subject"][14] = "|site_name|: Invitation to read |member_name|'s Classified Ad";
$messages["body"][14] = "Hi |recipient_name|,

You have received an invitation to read classified ad |classified_ad_name| from |member_name|.
Please, check your mailbox at |site_name|.

regards,
|site_name| Support.";

$messages["subject"][15] = "|subject|";
$messages["body"][15] = "|body|

To read classified ad, please, click on the following link:
|link|";

$messages["subject"][16] = "|site_name|: Invitation to attend |member_name|'s Event";
$messages["body"][16] = "Hi |recipient_name|,

You have received an invitation to attend event |event_name| from |member_name|.
Please, check your mailbox at |site_name|.

regards,
|site_name| Support.";

$messages["subject"][17] = "|subject|";
$messages["body"][17] = "|body|

To view details of this event, please, click on the following link:
|link|";

$messages["subject"][18] = "|site_name|: Invitation to join |member_name|'s Club";
$messages["body"][18] = "Hi |recipient_name|,

You have received an invitation to join |club_name| Club from |member_name|.
Please, check your mailbox at |site_name|.

regards,
|site_name| Support.";

$messages["subject"][19] = "|subject|";
$messages["body"][19] = "|body|

To view details of this club, please, click on the following link:
|link|";

$messages["subject"][20] = "|site_name|: Saved Search Match Alert";
$messages["body"][20] = "Hi |recipient_name|,

New items were found at |site_name|, which match your saved search |search_name|.

|items|

regards,
|site_name| Support.";

$messages["subject"][21] = "|site_name|: Featured Status Availability";
$messages["body"][21] = "Hi |recipient_name|,

We have got an awaiting position in our featured category for your |item_type| |item_name|.
You can now upgrade your |item_type| to the featured status.

regards,
|site_name| Support.";

$messages["subject"][22] = "|site_name|: Your Blog Has Been Deleted";
$messages["body"][22] = "Hi |recipient_name|,

Your blog |name| has been deleted.
It may have been deleted by moderator or by yourself.
Since this was a featured blog, please use the link below to stop any further charges.

|link|

regards,
|site_name| Support.";

$messages["subject"][23] = "|site_name|: Your Blog Has Been Deleted";
$messages["body"][23] = "Hi |recipient_name|,

Your blog |name| has been deleted.
It may have been deleted by moderator or by yourself.

regards,
|site_name| Support.";

$messages["subject"][24] = "|site_name|: Your Classified Ad Has Been Deleted";
$messages["body"][24] = "Hi |recipient_name|,

Your classified ad |name| has been deleted.
It may have been deleted by moderator or by yourself or it has just expired.
Since this was a featured classified ad, please use the link below to stop any further charges.

|link|

regards,
|site_name| Support.";

$messages["subject"][25] = "|site_name|: Your Classified Ad Has Been Deleted";
$messages["body"][25] = "Hi |recipient_name|,

Your classified ad |name| has been deleted.
It may have been deleted by moderator or by yourself or it has just expired.

regards,
|site_name| Support.";

$messages["subject"][26] = "|site_name|: Your Event Has Been Deleted";
$messages["body"][26] = "Hi |recipient_name|,

Your event |name| has been deleted.
It may have been deleted by moderator or by yourself or it has just expired.
Since this was a featured event, please use the link below to stop any further charges.

|link|

regards,
|site_name| Support.";

$messages["subject"][27] = "|site_name|: Your Event Has Been Deleted";
$messages["body"][27] = "Hi |recipient_name|,

Your event |name| has been deleted.
It may have been deleted by moderator or by yourself or it has just expired.

regards,
|site_name| Support.";

$messages["subject"][28] = "|site_name|: Your Club Has Been Deleted";
$messages["body"][28] = "Hi |recipient_name|,

Your club |name| has been deleted.
It may have been deleted by moderator or by yourself.
Since this was a featured club, please use the link below to stop any further charges.

|link|

regards,
|site_name| Support.";

$messages["subject"][29] = "|site_name|: Your Club Has Been Deleted";
$messages["body"][29] = "Hi |recipient_name|,

Your club |name| has been deleted.
It may have been deleted by moderator or by yourself.

regards,
|site_name| Support.";

$messages["subject"][30] = "|site_name|: Your Classified Ad Has Expired";
$messages["body"][30] = "Hi |recipient_name|,

Your classified ad |name| has recently expired.
You can still access it on \"My Classifieds\" page at |site_name|.
Since this was a featured classified ad, please use the link below to stop any further charges.

|link|

regards,
|site_name| Support.";

$messages["subject"][31] = "|site_name|: Your Classified Ad Has Expired";
$messages["body"][31] = "Hi |recipient_name|,

Your classified ad |name| has recently expired.
You can still access it on \"My Classifieds\" page at |site_name|.

regards,
|site_name| Support.";

$messages["subject"][32] = "|site_name|: Your Event Has Expired";
$messages["body"][32] = "Hi |recipient_name|,

Your event |name| has recently expired.
You can still access it on \"My Events\" page at |site_name|.
Since this was a featured event, please use the link below to stop any further charges.

|link|

regards,
|site_name| Support.";

$messages["subject"][33] = "|site_name|: Your Event Has Expired";
$messages["body"][33] = "Hi |recipient_name|,

Your event |name| has recently expired.
You can still access it on \"My Events\" page at |site_name|.

regards,
|site_name| Support.";

$messages["subject"][34] = "|site_name|: Your Account Has Been Upgraded";
$messages["body"][34] = "Hi |recipient_name|,

Thank you for upgrading your account.
Now more features will become available for you and we hope you will like networking at |site_name|.

regards,
|site_name| Support";

$messages["subject"][35] = "|site_name|: Account Membership Renewed";
$messages["body"][35] = "Hi |recipient_name|,

Thank you for renewing your account.
We hope you enjoy networking at |site_name| and hope to see you on our site soon.

regards,
|site_name| Support";

$messages["subject"][36] = "|site_name|: Your Blog Is Featured Now";
$messages["body"][36] = "Hi |recipient_name|,

Thank you for making your blog featured.
It will now appear at the top of blogs section pages, attracting attention of all of our members.

regards,
|site_name| Support";

$messages["subject"][37] = "|site_name|: Your Featured Blog Renewed";
$messages["body"][37] = "Hi |recipient_name|,

Thank you for renewing you featured blog.

regards,
|site_name| Support";

$messages["subject"][38] = "|site_name|: Your Classified Ad Is Featured Now";
$messages["body"][38] = "Hi |recipient_name|,

Thank you for making your classified ad featured.
It will now appear at the top of your category, attracting attention of all category visitors.

regards,
|site_name| Support";

$messages["subject"][39] = "|site_name|: Your Featured Classified Ad Renewed";
$messages["body"][39] = "Hi |recipient_name|,

Thank you for renewing you featured classified ad.

regards,
|site_name| Support";

$messages["subject"][40] = "|site_name|: Your Event Is Featured Now";
$messages["body"][40] = "Hi |recipient_name|,

Thank you for making your event featured.
It will now appear at the top of your category, attracting attention of all category visitors.

regards,
|site_name| Support";

$messages["subject"][41] = "|site_name|: Your Featured Event Renewed";
$messages["body"][41] = "Hi |recipient_name|,

Thank you for renewing you featured event.

regards,
|site_name| Support";

$messages["subject"][42] = "|site_name|: Your Club Is Featured Now";
$messages["body"][42] = "Hi |recipient_name|,

Thank you for making your club featured.
It will now appear at the top of your category, attracting attention of all category visitors.

regards,
|site_name| Support";

$messages["subject"][43] = "|site_name|: Your Featured Club Renewed";
$messages["body"][43] = "Hi |recipient_name|,

Thank you for renewing you featured event.

regards,
|site_name| Support";

$messages["subject"][44] = "Your Account Has Been Approved!";
$messages["body"][44] = "Hi |recipient_name|,

Your account at |site_name| has been recently approved by site administrator.
You can now access all sections of a site and start building your network.

regards,
|site_name| support";

$messages["subject"][45] = "|site_name|: Email changed.";
$messages["body"][45] = "Hi |recipient_name|,

You recently made a change to your email address.
This email is sent automatically to inform you about new login details.

Login: |login|
Password: |password|

Please, click on the link below to confirm email change:
|link|

regards,
|site_name| support";

$messages["subject"][100] = "Invitation accepted.";
$messages["body"][100] = "Member |member_name| accepted your invitation to join your network. Member is added to your 1st handshakes friends.";

$messages["subject"][101] = "Invitation declined.";
$messages["body"][101] = "Member |member_name| declined your invitation to join your network.";

$messages["subject"][102] = "Request to join |club_name| Club.";
$messages["body"][102] = "Member |member_name| has recently sent a request to join your club |club_name|. You may either accept or decline this request.";

$messages["subject"][103] = "Request to join |club_name| Club.";
$messages["body"][103] = "Member |member_name| has recently sent an invitation to join your club |club_name| to member |member_name2|. Invited member accepted invitation and this request was automatically sent to you as you are the owner of this club. You may either accept or decline this request.";

$messages["subject"][104] = "Request accepted.";
$messages["body"][104] = "Member |member_name| accepted your request to join club |club_name|.";

$messages["subject"][105] = "Request declined.";
$messages["body"][105] = "Member |member_name| declined your request to join club |club_name|.";

$messages["subject"][106] = "Member |member_name| has invited you to join their network.";
$messages["body"][106] = "|custommessage|";

$messages["subject"][107] = "Invitation to visit |member_name|'s Blog";
$messages["body"][107] = "This is an invitation to visit Blog \"|blog_name|\" from |member_name|.
To view this blog, please click on the button \"View\" below.";

$messages["subject"][108] = "Invitation to read |member_name|'s Classified Ad";
$messages["body"][108] = "This is an invitation to read Classified Ad \"|classified_ad_name|\" from |member_name|.
To read this classified ad, please click on the button \"View\" below.";

$messages["subject"][109] = "Invitation to attend |member_name|'s Event";
$messages["body"][109] = "This is an invitation to attend Event \"|event_name|\" from |member_name|.
To view details of this event, please click on the button \"View\" below.";

$messages["subject"][110] = "Invitation to join |member_name|'s Club";
$messages["body"][110] = "This is an invitation to join \"|club_name|\" Club from |member_name|.
To view details of this club, please click on the button \"View\" below.";


?>